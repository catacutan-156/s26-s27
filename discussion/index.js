//GOAL: Create a server using "Express.js"

// 1. Use the require() directive to acquire the Express library and its utilities.
	const express = require("express");

// 2. Create a server using only Express.
	const server = express();
	// express() will allow us to create an express application.

// 3. Identify a port/address that will serve/host this newly created connection/app.
	const address = 3000;

// 4. Bind the application to the desired designated port using listen(), create a method that will display a response that the connection has been established.
	server.listen(address, () => {
		console.log(`Server is running on port: ${address}`);
	});
	
